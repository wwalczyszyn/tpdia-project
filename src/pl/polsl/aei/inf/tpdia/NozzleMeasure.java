package pl.polsl.aei.inf.tpdia;
import java.util.Date;


public class NozzleMeasure {
	private Integer idNozzle;
	private Integer idTank;
	private Date time;
	private Double literCounter;
	private Double totalCounter;
	private Boolean refuelling;
	
	public Integer getIdNozzle() {
		return idNozzle;
	}
	public void setIdNozzle(Integer idNozzle) {
		this.idNozzle = idNozzle;
	}
	public Integer getIdTank() {
		return idTank;
	}
	public void setIdTank(Integer idTank) {
		this.idTank = idTank;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public Double getLiterCounter() {
		return literCounter;
	}
	public void setLiterCounter(Double literCounter) {
		this.literCounter = literCounter;
	}
	public Double getTotalCounter() {
		return totalCounter;
	}
	public void setTotalCounter(Double totalCounter) {
		this.totalCounter = totalCounter;
	}
	public Boolean getRefuelling() {
		return refuelling;
	}
	public void setRefuelling(Boolean refuelling) {
		this.refuelling = refuelling;
	}
}
