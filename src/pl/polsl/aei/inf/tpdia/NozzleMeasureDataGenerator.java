package pl.polsl.aei.inf.tpdia;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import com.opencsv.CSVReader;

public class NozzleMeasureDataGenerator implements Iterator<NozzleMeasure> {
	private Iterator<String[]> csvIterator;

	public NozzleMeasureDataGenerator(InputStream input, char separator) {
		CSVReader csvReader = new CSVReader(new InputStreamReader(input),
				separator);
		csvIterator = csvReader.iterator();
	}

	@Override
	public boolean hasNext() {
		return csvIterator.hasNext();
	}

	@Override
	public NozzleMeasure next() {
		if (hasNext()) {
			String[] data = csvIterator.next();

			NozzleMeasure nozzleMeasure = new NozzleMeasure();
			if (StringUtils.isNotBlank(data[0])) {
				Date time = getTime(data[0]);
				nozzleMeasure.setTime(time);
			}

			if (StringUtils.isNotBlank(data[2])) {
				nozzleMeasure.setIdNozzle(Integer.parseInt(data[2]));
			}

			if (StringUtils.isNotBlank(data[3])) {
				nozzleMeasure.setIdTank(Integer.parseInt(data[3]));
			}

			if (StringUtils.isNotBlank(data[4])) {
				String doubleNumber = data[4];
				doubleNumber = StringUtils.replaceChars(doubleNumber, ',', '.');
				nozzleMeasure.setLiterCounter(Double.parseDouble(doubleNumber));
			}

			if (StringUtils.isNotBlank(data[5])) {
				String doubleNumber = data[5];
				doubleNumber = StringUtils.replaceChars(doubleNumber, ',', '.');
				nozzleMeasure.setTotalCounter(Double.parseDouble(doubleNumber));
			}

			if (StringUtils.isNotBlank(data[6])) {
				nozzleMeasure.setRefuelling(data[6].equals("0"));
			}

			return nozzleMeasure;
		}

		return null;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
	
	private Date getTime(String data) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm");
		Date time = null;
		try {
			time = simpleDateFormat.parse(data);
		} catch (ParseException e) {
			return null;
		}
		return time;
	}
}
