package pl.polsl.aei.inf.tpdia;

import java.util.Date;

public class Difference {
    private Double sumOfTankValues;
    private Double sumOfNozzleValues;
    private Double sumOfRefuels;
    private Double difference;
    private Date startTime;
    private Date endTime;
    private Double cumulativeValue;
    private Double aproximatedValue;
    private Double interpolatedValue;
    private String status;

    public Double getSumOfTankValues() {
        return sumOfTankValues;
    }

    public void setSumOfTankValues(Double sumOfTankValues) {
        this.sumOfTankValues = sumOfTankValues;
    }

    public Double getSumOfNozzleValues() {
        return sumOfNozzleValues;
    }

    public void setSumOfNozzleValues(Double sumOfNozzleValues) {
        this.sumOfNozzleValues = sumOfNozzleValues;
    }

    public Double getSumOfRefuels() {
        return sumOfRefuels;
    }

    public void setSumOfRefuels(Double sumOfRefuels) {
        this.sumOfRefuels = sumOfRefuels;
    }

    public Double getDifference() {
        return difference;
    }

    public void setDifference(Double difference) {
        this.difference = difference;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Double getCumulativeValue() {
        return cumulativeValue;
    }

    public void setCumulativeValue(Double cumulativeValue) {
        this.cumulativeValue = cumulativeValue;
    }

    public Double getAproximatedValue() {
        return aproximatedValue;
    }

    public void setAproximatedValue(Double aproximatedValue) {
        this.aproximatedValue = aproximatedValue;
    }

    public Double getInterpolatedValue() {
        return interpolatedValue;
    }

    public void setInterpolatedValue(Double interpolatedValue) {
        this.interpolatedValue = interpolatedValue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
