package pl.polsl.aei.inf.tpdia;

import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DataLoader {

    public static void main(String[] args) {
        List<TankMeasure> tankMeasures = new LinkedList<>();
        List<NozzleMeasure> nozzleMeasures = new LinkedList<>();
        List<Refuel> refuels = new LinkedList<>();

        File tankMeasureCsv = new File("tankMeasures.log");
        File nozzleMeasureCsv = new File("nozzleMeasures.log");
        File refuelCsv = new File("refuel.log");

        try {
            TankMeasureDataGenerator tankMeasureDataGenerator = new TankMeasureDataGenerator(new FileInputStream(tankMeasureCsv), ';');
            while (tankMeasureDataGenerator.hasNext()) {
                tankMeasures.add(tankMeasureDataGenerator.next());
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Collections.sort(tankMeasures, (data1, data2) ->
                data1.getTime().after(data2.getTime()) ? 1 :
                        data1.getTime().before(data2.getTime()) ? -1 : 0);

        try {
            NozzleMeasureDataGenerator nozzleMeasureDataGenerator = new NozzleMeasureDataGenerator(new FileInputStream(nozzleMeasureCsv), ';');
            while (nozzleMeasureDataGenerator.hasNext()) {
                nozzleMeasures.add(nozzleMeasureDataGenerator.next());
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Collections.sort(nozzleMeasures, (data1, data2) ->
                data1.getTime().after(data2.getTime()) ? 1 :
                        data1.getTime().before(data2.getTime()) ? -1 : 0);

        try {
            RefuelDataGenerator refuelDataGenerator = new RefuelDataGenerator(new FileInputStream(refuelCsv), ';');
            while (refuelDataGenerator.hasNext()) {
                refuels.add(refuelDataGenerator.next());
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Collections.sort(refuels, (data1, data2) ->
                data1.getTime().after(data2.getTime()) ? 1 :
                        data1.getTime().before(data2.getTime()) ? -1 : 0);

        try {
            File outputFile = new File("results.csv");
            if (outputFile.exists()) {
                outputFile.delete();
            }
            outputFile.createNewFile();

            PrintWriter writer = new PrintWriter(outputFile);

            writer.print("Tank id");
            writer.print(';');
            writer.print("Start time");
            writer.print(';');
            writer.print("End time");
            writer.print(';');
            writer.print("Tank delta");
            writer.print(';');
            writer.print("Nozzle counter");
            writer.print(';');
            writer.print("Refuel counter");
            writer.print(';');
            writer.print("Total difference");
            writer.print(';');
            writer.print("Cumulative difference");
            //writer.print(';');
            //writer.print("Approximated difference");
            writer.print(';');
            writer.print("Interpolated difference");
            writer.print(';');
            writer.println("Status");

            double timeInHours = 1;

            for (int i = 1; i < 5; i++) {
                List<Difference> differences = Counter.getDifferences(tankMeasures, nozzleMeasures, refuels, (long) (timeInHours * 60 * 60 * 1000), i);

                for (Difference difference : differences) {
                    writer.print(i);
                    writer.print(';');
                    writer.print(difference.getStartTime());
                    writer.print(';');
                    writer.print(difference.getEndTime());
                    writer.print(';');
                    writer.print(StringUtils.replaceChars(difference.getSumOfTankValues().toString(), '.', ','));
                    writer.print(';');
                    writer.print(StringUtils.replaceChars(difference.getSumOfNozzleValues().toString(), '.', ','));
                    writer.print(';');
                    writer.print(StringUtils.replaceChars(difference.getSumOfRefuels().toString(), '.', ','));
                    writer.print(';');
                    writer.print(StringUtils.replaceChars(difference.getDifference().toString(), '.', ','));
                    writer.print(';');
                    writer.print(StringUtils.replaceChars(difference.getCumulativeValue().toString(), '.', ','));
                    //writer.print(';');
                    //writer.print(StringUtils.replaceChars(difference.getAproximatedValue().toString(), '.', ','));
                    writer.print(';');
                    writer.print(StringUtils.replaceChars(difference.getInterpolatedValue().toString(), '.', ','));
                    writer.print(';');
                    writer.println(difference.getStatus());
                }
            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Done");

    }

}
