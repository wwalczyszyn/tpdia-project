package pl.polsl.aei.inf.tpdia;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import com.opencsv.CSVReader;

public class RefuelDataGenerator implements Iterator<Refuel> {
	private Iterator<String[]> csvIterator;

	public RefuelDataGenerator(InputStream input, char separator) {
		CSVReader csvReader = new CSVReader(new InputStreamReader(input),
				separator);
		csvIterator = csvReader.iterator();
	}

	@Override
	public boolean hasNext() {
		return csvIterator.hasNext();
	}

	@Override
	public Refuel next() {
		if (hasNext()) {
			String[] data = csvIterator.next();

			Refuel refuel = new Refuel();
			if (StringUtils.isNotBlank(data[0])) {
				Date time = getTime(data[0]);
				refuel.setTime(time);
			}

			if (StringUtils.isNotBlank(data[1])) {
				refuel.setId(Integer.parseInt(data[1]));
			}

			if (StringUtils.isNotBlank(data[2])) {
				String doubleNumber = data[2];
				doubleNumber = StringUtils.replaceChars(doubleNumber, ',', '.');
				refuel.setVolume(Double.parseDouble(doubleNumber));
			}

			if (StringUtils.isNotBlank(data[3])) {
				String doubleNumber = data[3];
				doubleNumber = StringUtils.replaceChars(doubleNumber, ',', '.');
				refuel.setSpeed(Double.parseDouble(doubleNumber));
			}

			return refuel;
		}

		return null;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
	
	private Date getTime(String data) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		Date time = null;
		try {
			time = simpleDateFormat.parse(data);
		} catch (ParseException e) {
			return null;
		}
		return time;
	}
}
