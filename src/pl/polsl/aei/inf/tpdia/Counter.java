package pl.polsl.aei.inf.tpdia;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

import java.util.*;
import java.util.stream.Collectors;

public class Counter {

    public static List<Difference> getDifferences(List<TankMeasure> tankMeasures, List<NozzleMeasure> nozzleMeasures,
                                                  List<Refuel> refuels, Long diffTime, int tankId) {
        List<Difference> differences = new ArrayList<>();

        Date startTime = nozzleMeasures.get(0).getTime();
        startTime.setHours(0);
        startTime.setMinutes(0);
        startTime.setSeconds(0);
        Date endTime = new Date(startTime.getTime() + diffTime);
        double cumulativeValue = 0;

        while (endTime.before(nozzleMeasures.get(nozzleMeasures.size() - 1).getTime())) {
            Difference difference = getDifference(tankMeasures, nozzleMeasures, refuels, startTime, endTime, tankId);

            cumulativeValue += difference.getDifference();
            difference.setCumulativeValue(cumulativeValue);
            differences.add(difference);

            startTime = endTime;
            endTime = new Date(startTime.getTime() + diffTime);
        }

        approximatePhaseOne(differences);
        approximatePhaseTwo(differences);

        interpolate(differences);

        return differences;
    }

    public static void approximatePhaseOne(List<Difference> differences) {
        Difference first = differences.get(0);
        first.setAproximatedValue(first.getCumulativeValue());

        Difference second = differences.get(1);
        second.setAproximatedValue(second.getCumulativeValue());

        Difference beforeLast = differences.get(differences.size() - 2);
        beforeLast.setAproximatedValue(beforeLast.getCumulativeValue());

        Difference last = differences.get(differences.size() - 1);
        last.setAproximatedValue(last.getCumulativeValue());

        for (int i = 2; i < differences.size() - 2; i++) {
            Difference left2 = differences.get(i - 2);
            Difference left1 = differences.get(i - 1);
            Difference center = differences.get(i);
            Difference right1 = differences.get(i + 1);
            Difference right2 = differences.get(i + 2);

            double approx = (left2.getCumulativeValue() + left1.getCumulativeValue() + center.getCumulativeValue() + right1.getCumulativeValue() + right2.getCumulativeValue()) / 5;
            double diffLeft2 = Math.abs(approx - left2.getCumulativeValue());
            double diffLeft1 = Math.abs(approx - left1.getCumulativeValue());
            double diffCenter = Math.abs(approx - center.getCumulativeValue());
            double diffRight1 = Math.abs(approx - right1.getCumulativeValue());
            double diffRight2 = Math.abs(approx - right2.getCumulativeValue());

            double maxDiff = Math.max(Math.max(Math.max(diffLeft2, diffLeft1), Math.max(diffRight1, diffRight2)), diffCenter);

            if (maxDiff == diffLeft2) {
                approx = (left1.getCumulativeValue() + center.getCumulativeValue() + right1.getCumulativeValue() + right2.getCumulativeValue()) / 4;
            } else if (maxDiff == diffLeft1) {
                approx = (left2.getCumulativeValue() + center.getCumulativeValue() + right1.getCumulativeValue() + right2.getCumulativeValue()) / 4;
            } else if (maxDiff == diffCenter) {
                approx = (left2.getCumulativeValue() + left1.getCumulativeValue() + right1.getCumulativeValue() + right2.getCumulativeValue()) / 4;
            } else if (maxDiff == diffRight1) {
                approx = (left2.getCumulativeValue() + left1.getCumulativeValue() + center.getCumulativeValue() + right2.getCumulativeValue()) / 4;
            } else if (maxDiff == diffRight2) {
                approx = (left2.getCumulativeValue() + left1.getCumulativeValue() + center.getCumulativeValue() + right1.getCumulativeValue()) / 4;
            }

            center.setAproximatedValue(approx);
        }
    }

    public static void approximatePhaseTwo(List<Difference> differences) {
        int step = differences.size() / 28;

        for (int i = step; i < differences.size() - step; i++) {
            double sum = 0;
            for (int j = (i - step); j <= (i + step); j++) {
                sum += differences.get(j).getAproximatedValue();
            }

            double approx = sum / ((step * 2) + 1);

            differences.get(i).setAproximatedValue(approx);
        }
    }

    public static void interpolate(List<Difference> differences) {
        List<Double> x = new ArrayList<>();
        List<Double> y = new ArrayList<>();

        x.add(0.0);
        y.add(differences.get(0).getAproximatedValue());

        if (differences.size() > 2) {
            double startValue = differences.get(0).getAproximatedValue();
            double endValue = differences.get(2).getAproximatedValue();

            //double lastDelta = startValue != 0 ? endValue / startValue : 1;
            double lastDelta;
            if (startValue == 0 && endValue == 0) {
                lastDelta = 1;
            } else {
                lastDelta = endValue - startValue;
            }


            int i;
            for (i = 0; i < differences.size() - 4; i += 2) {
                int start = i;
                int end = i + 2;

                startValue = differences.get(start).getAproximatedValue();
                endValue = differences.get(end).getAproximatedValue();

                //double delta = startValue != 0 ? endValue / startValue : 1;
                double delta;
                if (startValue == 0 && endValue == 0) {
                    delta = 0;
                } else {
                    delta = endValue - startValue;
                }

                double deltaChange;
                if (lastDelta == 0 && delta == 0) {
                    deltaChange = 1;
                } else {
                    deltaChange = Math.abs(lastDelta - delta);
                }
                //double deltaChange = (lastDelta != 0 ? Math.abs(delta / lastDelta) : delta) * 100;

                if (deltaChange > 10) {
                    x.add((double) end);
                    y.add(differences.get(end).getAproximatedValue());

                    lastDelta = delta;
                }
            }
        }

        x.add((double) (differences.size() - 1));
        y.add(differences.get(differences.size() - 1).getAproximatedValue());

        for (int z = 0; z < x.size() - 1; z++) {
            double x1 = x.get(z);
            double x2 = x.get(z + 1);

            double y1 = y.get(z);
            double y2 = y.get(z + 1);

            String state;
            double delta = y2 - y1;
            if (Math.abs(delta) > 50) {
                if (delta > 0) {
                    state = "NADMIAR";
                } else {
                    state = "WYCIEK";
                }
            } else {
                state = "OK";
            }

            for (int t = (int) x1; t <= x2; t++) {
                differences.get(t).setStatus(state);
            }
        }

        LinearInterpolator linearInterpolator = new LinearInterpolator();

        double[] xPrimitive = ArrayUtils.toPrimitive(x.toArray(new Double[x.size()]));
        double[] yPrimitive = ArrayUtils.toPrimitive(y.toArray(new Double[y.size()]));
        PolynomialSplineFunction function = linearInterpolator.interpolate(xPrimitive, yPrimitive);

        for (int j = 0; j < differences.size(); j++) {
            differences.get(j).setInterpolatedValue(function.value(j));
        }
    }

    private static double getTankSumOfVolume(List<TankMeasure> tankMeasures, Date startTime, Date endTime) {
        double delta = 0;

        for (int i = 0; i < tankMeasures.size() - 1; i++) {
            TankMeasure measureOne = tankMeasures.get(i);
            TankMeasure measureTwo = tankMeasures.get(i + 1);

            Date timeOne = measureOne.getTime();
            Date timeTwo = measureTwo.getTime();
            if (!timeOne.before(startTime) && !timeOne.after(endTime) && !timeTwo.before(startTime)
                    && !timeTwo.after(endTime)) {
                delta += (measureTwo.getVolume() - measureOne.getVolume());
            } else if (timeOne.after(endTime) && timeTwo.after(endTime)) {
                break;
            }
        }

        return delta;
    }

    private static Double getNozzleSumOfVolume(List<NozzleMeasure> nozzleMeasures, Date startTime, Date endTime) {
        double totalDelta = 0;

        for (int nozzleId = 12; nozzleId < 30; nozzleId++) {
            final int id = nozzleId;

            List<NozzleMeasure> singleNozzleMeasures = nozzleMeasures.stream()
                    .filter(measure -> measure.getIdNozzle() == id)
                    .collect(Collectors.toCollection(() -> new LinkedList<>()));

            double delta = 0;

            for (int i = 0; i < singleNozzleMeasures.size() - 1; i++) {
                NozzleMeasure measureOne = singleNozzleMeasures.get(i);
                NozzleMeasure measureTwo = singleNozzleMeasures.get(i + 1);

                Date timeOne = measureOne.getTime();
                Date timeTwo = measureTwo.getTime();
                if (!timeOne.before(startTime) && !timeOne.after(endTime) && !timeTwo.before(startTime)
                        && !timeTwo.after(endTime)) {
                    delta += (measureTwo.getTotalCounter() - measureOne.getTotalCounter());
                } else if (timeOne.after(endTime) && timeTwo.after(endTime)) {
                    break;
                }
            }

            totalDelta += delta;
        }

        return totalDelta;
    }

    private static Double getRefuelSumOfValue(List<Refuel> refuels, Date startTime, Date endTime) {
        Double result = 0d;
        for (Refuel refuel : refuels) {
            Date refuelTime = new Date(refuel.getTime().getTime() - (60 * 1000)); //refuel.getTime();
            Date refuelEnd = new Date(
                    (long) (refuelTime.getTime() + (refuel.getVolume() / refuel.getSpeed()) * 60 * 1000));
            if ((!refuelTime.before(startTime) && !refuelTime.after(endTime)) //begins in time
                    || (!refuelEnd.before(startTime) && !refuelEnd.after(endTime)) //end in time
                    || (refuelTime.before(startTime) && refuelEnd.after(endTime))) { //during in time
                double time = Math.min(refuelEnd.getTime(), endTime.getTime())
                        - Math.max(refuelTime.getTime(), startTime.getTime());
                time /= (60 * 1000); // minutes

                result += time * refuel.getSpeed();//Math.min(time * refuel.getSpeed(), refuel.getVolume());
            } else if (refuelTime.after(endTime)) {
                break;
            }
        }
        return result;
    }

    private static Difference getDifference(List<TankMeasure> tankMeasures, List<NozzleMeasure> nozzleMeasures,
                                            List<Refuel> refuels, Date startTime, Date endTime, int tankId) {
        Difference difference = new Difference();

        List<TankMeasure> singleTankMeasures = tankMeasures.stream().filter(measure -> measure.getId() == tankId)
                .collect(Collectors.toCollection(() -> new LinkedList<>()));
        List<NozzleMeasure> singleNozzleMeasures = nozzleMeasures.stream()
                .filter(measure -> measure.getIdTank() == tankId)
                .collect(Collectors.toCollection(() -> new LinkedList<>()));
        List<Refuel> singleRefuelMeasures = refuels.stream().filter(measure -> measure.getId() == tankId)
                .collect(Collectors.toCollection(() -> new LinkedList<>()));

        difference.setSumOfTankValues(getTankSumOfVolume(singleTankMeasures, startTime, endTime));
        difference.setSumOfNozzleValues(getNozzleSumOfVolume(singleNozzleMeasures, startTime, endTime));
        difference.setSumOfRefuels(getRefuelSumOfValue(singleRefuelMeasures, startTime, endTime));
        difference.setStartTime(startTime);
        difference.setEndTime(endTime);
        difference.setDifference(difference.getSumOfTankValues() - difference.getSumOfRefuels() + difference.getSumOfNozzleValues());

        return difference;
    }

}
