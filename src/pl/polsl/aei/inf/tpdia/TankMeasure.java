package pl.polsl.aei.inf.tpdia;
import java.util.Date;


public class TankMeasure {
	private Integer id;
	private Date time;
	private Double volume;
	private Long temp;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public Double getVolume() {
		return volume;
	}
	public void setVolume(Double volume) {
		this.volume = volume;
	}
	public Long getTemp() {
		return temp;
	}
	public void setTemp(Long temp) {
		this.temp = temp;
	}
}
