package pl.polsl.aei.inf.tpdia;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import com.opencsv.CSVReader;

public class TankMeasureDataGenerator implements Iterator<TankMeasure> {
	private Iterator<String[]> csvIterator;

	public TankMeasureDataGenerator(InputStream input, char separator) {
		CSVReader csvReader = new CSVReader(new InputStreamReader(input),
				separator);
		csvIterator = csvReader.iterator();
	}

	@Override
	public boolean hasNext() {
		return csvIterator.hasNext();
	}

	@Override
	public TankMeasure next() {
		if (hasNext()) {
			String[] data = csvIterator.next();

			TankMeasure tankMeasure = new TankMeasure();
			if (StringUtils.isNotBlank(data[0])) {
				Date time = getTime(data[0]);
				tankMeasure.setTime(time);

			}

			if (StringUtils.isNotBlank(data[3])) {
				tankMeasure.setId(Integer.parseInt(data[3]));
			}

			if (StringUtils.isNotBlank(data[5])) {
				String doubleNumber = data[5];
				doubleNumber = StringUtils.replaceChars(doubleNumber, ',', '.');
				tankMeasure.setVolume(Double.parseDouble(doubleNumber));
			}

			if (StringUtils.isNotBlank(data[6])) {
				tankMeasure.setTemp(Long.parseLong(data[6]));
				//doubleNumber = StringUtils.replaceChars(doubleNumber, ',', '.');
				//tankMeasure.setTemperature(Double.parseDouble(doubleNumber));
			}

			return tankMeasure;
		}

		return null;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
	
	private Date getTime(String data) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		Date time = null;
		try {
			time = simpleDateFormat.parse(data);
		} catch (ParseException e) {
			return null;
		}
		return time;
	}
}
